<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Invoice;
use App\Entity\User;
use App\Repository\Filter\InvoiceFilter;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Invoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invoice[]    findAll()
 * @method Invoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Invoice::class);
    }

    /**
     * @param User $user
     *
     * @return Invoice[]
     */
    public function findByUser(User $user): array
    {
        $qb = $this->createQueryBuilder('i');

        return $qb
            ->innerJoin('i.order', 'o')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('o.user', $user)
                )
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     *
     * @return Invoice[]
     */
    public function findById(int $id): array
    {
        $qb = $this->createQueryBuilder('i');

        return $qb
            ->where(
                $qb->expr()->eq('i.id', $id)
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * Find invoices that should be sync to the Moserholding bookkeeping, we always send the invoices of the last week.
     *
     * @return Invoice[]
     */
    public function findMoserholdingBookkeeping(): array
    {
        $qb = $this->createQueryBuilder('i');

        // Do not sync orders of today to avoid race conditions
        // Also gives us some time fix broken orders/invoices before we sync them
        $onlySyncBefore = (new DateTimeImmutable())->setTime(0, 0);

        return $qb
            ->innerJoin('i.order', 'o')
            ->setParameter('created', $onlySyncBefore, \Doctrine\DBAL\Types\Type::DATETIME_IMMUTABLE)
            ->where(
                $qb->expr()->isNull('i.syncedToMoserholdingBookkeeping'),
                $qb->expr()->lt('i.created', ':created')
            )
            ->orderBy('i.created', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByFilter(InvoiceFilter $invoiceFilter)
    {
        $qb = $this->createQueryBuilder('i')
            ->leftJoin('i.order', 'o')
            ->leftJoin('o.user', 'u');

        $constraints = [];

        if ($invoiceFilter->getFilter() !== null) {
            $constraints[] = $qb->expr()->like('i.id', ':filter');
            $constraints[] = $qb->expr()->like('u.company', ':filter');
            $constraints[] = $qb->expr()->like('i.invoiceNumber', ':filter');
            $constraints[] = $qb->expr()->like('i.intermediateAmount', ':filter');
            $constraints[] = $qb->expr()->like('i.totalAmount', ':filter');
            $constraints[] = $qb->expr()->like('i.created', ':filter');
            $qb->setParameter('filter', '%' . $invoiceFilter->getFilter() . '%');
        }

        if (!empty($constraints)) {
            $qb->where($qb->expr()->orX(...$constraints));
        }

        if ($invoiceFilter->getSort() !== null) {
            if ($invoiceFilter->getSort() == 'invoiceNumber') {
                $qb->orderBy('i.invoiceNumber', $invoiceFilter->getSortType());
            } elseif ($invoiceFilter->getSort() == 'intermediateAmount') {
                $qb->orderBy('i.intermediateAmount', $invoiceFilter->getSortType());
            } elseif ($invoiceFilter->getSort() == 'totalAmount') {
                $qb->orderBy('i.totalAmount', $invoiceFilter->getSortType());
            } elseif ($invoiceFilter->getSort() == 'created') {
                $qb->orderBy('i.created', $invoiceFilter->getSortType());
            } elseif ($invoiceFilter->getSort() == 'id') {
                $qb->orderBy('i.id', $invoiceFilter->getSortType());
            }
        }

        if ($invoiceFilter->getPage() !== null) {
            $qb->setFirstResult(($invoiceFilter->getPage() - 1) * $invoiceFilter->getRowsPerPage());
        }

        if ($invoiceFilter->getRowsPerPage() !== null) {
            $qb->setMaxResults($invoiceFilter->getRowsPerPage());
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function countRowsByFilter(InvoiceFilter $invoiceFilter)
    {
        $qb = $this->createQueryBuilder('i')
            ->select('count(i.id)')
            ->leftJoin('i.order', 'o')
            ->leftJoin('o.user', 'u');

        $constraints = [];

        if ($invoiceFilter->getFilter() !== null) {
            $constraints[] = $qb->expr()->like('u.company', ':filter');
            $constraints[] = $qb->expr()->like('i.invoiceNumber', ':filter');
            $constraints[] = $qb->expr()->like('i.intermediateAmount', ':filter');
            $constraints[] = $qb->expr()->like('i.totalAmount', ':filter');
            $constraints[] = $qb->expr()->like('i.created', ':filter');
            $qb->setParameter('filter', '%' . $invoiceFilter->getFilter() . '%');
        }

        if (!empty($constraints)) {
            $qb->where($qb->expr()->orX(...$constraints));
        }

        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }
}
