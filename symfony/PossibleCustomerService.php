<?php

declare(strict_types=1);

namespace App\Service\Customer;

use App\Entity\BillingAddress;
use App\Entity\EmailInbound;
use App\Entity\PossibleCustomer;
use App\Entity\User;
use App\Exception\InvalidPhoneNumberException;
use App\Repository\PossibleCustomerRepository;
use App\Utility\PhoneNumberUtility;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class PossibleCustomerService implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var PossibleCustomerRepository
     */
    protected $possibleCustomerRepository;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(
        PossibleCustomerRepository $possibleCustomerRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->em = $entityManager;
        $this->possibleCustomerRepository = $possibleCustomerRepository;
    }

    /**
     * @param array          $data
     * @param BillingAddress $billingAddress
     *
     * @return array
     */
    public function prepareCustomerDataAddCustomer($data, BillingAddress $billingAddress): array
    {
        return [
            'phone' => $data['phone'],
            'first_name' => $data['firstName'],
            'last_name' => $data['lastName'],
            'email' => $data['email'],
            'company' => $data['company'],
            'vat_id' => $data['vatId'],
            'website' => $data['website'],
            'description' => $data['description'],
            'tos' => $data['tos'],
            'privacy_policy' => $data['privacyPolicy'],
            'roles' => ['ROLE_USER', 'ROLE_CUSTOMER'],
            'company_phone' => $data['companyPhone'],
            'advertiser_id' => $data['advertiser'],
            'subscription_type' => null,
            'company_email' => $data['companyEmail'],
            'coupon_redeem_notification' => $data['couponRedeemNotification'],
            'daily_coupon_redeem_overview' => $data['dailyCouponRedeemOverview'],
            'weekly_coupon_redeem_overview' => $data['weeklyCouponRedeemOverview'],
            'monthly_coupon_redeem_overview' => $data['monthlyCouponRedeemOverview'],
            'is_bonus_card_enabled' => $data['isBonusCardEnabled'],
            'billingAddress' => $billingAddress,
        ];
    }

    /**
     * @param array            $userData
     * @param PossibleCustomer $possibleCustomer
     *
     * @return User $user
     */
    public function saveCustomer($userData, PossibleCustomer $possibleCustomer): User
    {
        $user = (new User())
            ->setPhone($userData['phone'])
            ->setFirstName($userData['first_name'])
            ->setLastName($userData['last_name'])
            ->setEmail($userData['email'])
            ->setCompany($userData['company'])
            ->setVatId($userData['vat_id'])
            ->setWebsite($userData['website'])
            ->setDescription($userData['description'])
            ->setTos($userData['tos'])
            ->setPrivacyPolicy($userData['privacy_policy'])
            ->setRoles($userData['roles'])
            ->setCompanyPhone($userData['company_phone'])
            ->setAdvertiser($userData['advertiser_id'])
            ->setSubscriptionType($userData['subscription_type'])
            ->setCompanyEmail($userData['company_email'])
            ->setCouponRedeemNotification($userData['coupon_redeem_notification'])
            ->setDailyCouponRedeemOverview($userData['daily_coupon_redeem_overview'])
            ->setWeeklyCouponRedeemOverview($userData['weekly_coupon_redeem_overview'])
            ->setMonthlyCouponRedeemOverview($userData['monthly_coupon_redeem_overview'])
            ->setBillingAddress($userData['billingAddress'])
            ->setIsBonusCardEnabled($userData['is_bonus_card_enabled']);

        $this->em->persist($user);
        $this->em->flush();

        $possibleCustomer->setUser($user);

        $this->em->persist($possibleCustomer);
        $this->em->flush();

        return $user;
    }

    /**
     * Parse the text body of an email and update the fields of the possible customer that are mentioned inside the mail.
     * This is used by our call center (MohoTel) to update the address data in our system. They can't call a webhook/open a url so this is the only possibility.
     *
     * @param EmailInbound $emailInbound Check /docs/Mail/Inbound/PossibleCustomerUpdate.md for a full example of the contained TextBody
     */
    public function updateByEmailInbound(EmailInbound $emailInbound)
    {
        $lines = preg_split('/\r\n|\r|\n/', $emailInbound->getTextBody());

        // Find the possible customer identifier inside the mail
        $source = null;
        $externalId = null;
        foreach ($lines as $line) {
            $line = trim($line);
            if (stripos($line, 'Source:') === 0) {
                $line = preg_replace('/^Source:/i', '', $line);
                $source = strtolower(trim($line));
            } elseif (stripos($line, 'ExternalId:') === 0) {
                $line = preg_replace('/^ExternalId:/i', '', $line);
                $externalId = trim($line);
            }

            if ($source !== null && $externalId !== null) {
                break;
            }
        }

        // Check if we found a possible customer
        $possibleCustomer = $this->possibleCustomerRepository->findOneBy(['source' => $source, 'externalId' => $externalId]);
        if ($source == null || $externalId === null || $possibleCustomer === null) {
            // Required data is missing or we couldn't find the possible customer in our database
            // We have to check for $source/$externalId === null because there are entries inside the database where source & external id is null
            $this->logger->critical(
                'Possible customer email update is missing the source/externalId or the possible customer does not exist inside our database.',
                ['lines' => $lines]
            );

            return;
        }

        // Parse/Find the new values for the possible customer fields
        $dataChanged = false;
        foreach ($lines as $line) {
            $line = trim($line);
            if (stripos($line, 'Company:') === 0) {
                $line = preg_replace('/^Company:/i', '', $line);
                $line = trim($line);
                // Handle the edge case that MohoTel sometimes sends us an HTML escaped & character.
                // To avoid side effects we do not use a generic approach like html_entity_decode()
                $line = str_replace('&amp;', '&', $line);
                $possibleCustomer->setCompany($line);
                $dataChanged = true;
            } elseif (stripos($line, 'FirstName:') === 0) {
                $line = preg_replace('/^FirstName:/i', '', $line);
                $possibleCustomer->setFirstName(trim($line));
                $dataChanged = true;
            } elseif (stripos($line, 'LastName:') === 0) {
                $line = preg_replace('/^LastName:/i', '', $line);
                $possibleCustomer->setLastName(trim($line));
                $dataChanged = true;
            } elseif (stripos($line, 'Email:') === 0) {
                $line = preg_replace('/^Email:/i', '', $line);
                $email = trim($line);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $possibleCustomer->setEmail($email);
                } else {
                    $this->logger->error(
                        'Possible customer email update contained an invalid Email: ' . $line,
                        [
                            'source' => $source,
                            'externalId' => $externalId,
                            'possibleCustomer' => $possibleCustomer->getId(),
                            'lines' => $lines,
                        ]
                    );
                }
                $dataChanged = true;
            } elseif (stripos($line, 'Street|HouseNumber:') === 0) {
                $line = preg_replace('/^Street\|HouseNumber:/i', '', $line);
                $possibleCustomer->setStreet(trim($line));
                $dataChanged = true;
            } elseif (stripos($line, 'Zip:') === 0) {
                $line = preg_replace('/^Zip:/i', '', $line);
                $possibleCustomer->setZip(trim($line));
                $dataChanged = true;
            } elseif (stripos($line, 'City:') === 0) {
                $line = preg_replace('/^City:/i', '', $line);
                $possibleCustomer->setCity(trim($line));
                $dataChanged = true;
            } elseif (stripos($line, 'ContactPhone:') === 0) {
                $line = preg_replace('/^ContactPhone:/i', '', $line);
                try {
                    $possibleCustomer->setPhoneContact($this->normalizeEmailInboundPhone($line));
                    $dataChanged = true;
                } catch (InvalidPhoneNumberException $invalidPhoneNumberException) {
                    $this->logger->error(
                        'Possible customer email update contained an invalid ContactPhone: ' . $line,
                        [
                            'source' => $source,
                            'externalId' => $externalId,
                            'possibleCustomer' => $possibleCustomer->getId(),
                            'lines' => $lines,
                            'invalidPhoneNumberException' => $invalidPhoneNumberException->getMessage(),
                        ]
                    );
                }
            } elseif (stripos($line, 'LoginPhone:') === 0) {
                $line = preg_replace('/^LoginPhone:/i', '', $line);
                try {
                    $possibleCustomer->setPhone($this->normalizeEmailInboundPhone($line));
                    $dataChanged = true;
                } catch (InvalidPhoneNumberException $invalidPhoneNumberException) {
                    $this->logger->error(
                        'Possible customer email update contained an invalid LoginPhone: ' . $line,
                        [
                            'source' => $source,
                            'externalId' => $externalId,
                            'possibleCustomer' => $possibleCustomer->getId(),
                            'lines' => $lines,
                            'invalidPhoneNumberException' => $invalidPhoneNumberException->getMessage(),
                        ]
                    );
                }
            }
        }

        if ($dataChanged) {
            $this->em->persist($possibleCustomer);
            $this->em->flush();
        }
    }

    /**
     * Normalize the phone number which is send by MohoTel.
     * They are not able to send us a format like "+431234" so we have to normalize it, before we write the phone numbers in our database.
     *
     * @param string $phone
     *
     * @throws InvalidPhoneNumberException
     *
     * @return string
     */
    protected function normalizeEmailInboundPhone(string $phone): string
    {
        $phone = trim($phone);
        // Convert 0512 to +43512 - MohoTel isn't able to send us an international phone number with +43
        if ($phone[0] === '0' && $phone[1] !== '0') {
            $phone = substr_replace($phone, '+43', 0, 1);
        }

        return PhoneNumberUtility::sanitize($phone);
    }
}
