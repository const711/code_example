<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\PossibleCustomer;
use App\Event\CreatingActiveCustomerEvent;
use App\Exception\Payment\InvalidVatIdException;
use App\Form\User\BillingAddressType;
use App\Membership\MembershipService;
use App\Payment\ProviderFactory;
use App\Payment\VatIdService;
use App\Repository\CountryRepository;
use App\Repository\MembershipTypeGroupRepository;
use App\Repository\PossibleCustomerRepository;
use App\Repository\UserRepository;
use App\Service\Address\AddressService;
use App\Service\Customer\PossibleCustomerService;
use App\Service\Marketing\PossibleCustomerMessageService;
use App\Service\Payment\PaymentDataService;
use App\Service\Store\StoreService;
use App\Utility\PhoneNumberUtility;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * @RouteResource("possible-customer")
 */
class PossibleCustomerController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var PossibleCustomerRepository
     */
    protected $possibleCustomerRepository;

    /**
     * @var PossibleCustomerMessageService
     */
    protected $possibleCustomerMessageService;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var PossibleCustomerService
     */
    protected $possibleCustomerService;

    /**
     * @var AddressService
     */
    protected $addressService;

    /**
     * @var StoreService
     */
    protected $storeService;

    /**
     * @var PaymentDataService
     */
    protected $paymentDataService;

    /**
     * @var ProviderFactory
     */
    protected $providerFactory;

    /**
     * @var MembershipTypeGroupRepository
     */
    protected $membershipTypeGroupRepository;

    /**
     * @var MembershipService
     */
    protected $membershipService;

    /**
     * @var VatIdService
     */
    protected $vatIdService;

    /**
     * @var CountryRepository
     */
    protected $countryRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(
        EntityManagerInterface $entityManager,
        PossibleCustomerRepository $possibleCustomerRepository,
        PossibleCustomerMessageService $possibleCustomerMessageService,
        UserRepository $userRepository,
        AddressService $addressService,
        StoreService $storeService,
        PaymentDataService $paymentDataService,
        ProviderFactory $providerFactory,
        PossibleCustomerService $possibleCustomerService,
        MembershipService $membershipService,
        VatIdService $vatIdService,
        CountryRepository $countryRepository,
        MembershipTypeGroupRepository $membershipTypeGroupRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $entityManager;
        $this->possibleCustomerRepository = $possibleCustomerRepository;
        $this->possibleCustomerMessageService = $possibleCustomerMessageService;
        $this->userRepository = $userRepository;
        $this->possibleCustomerService = $possibleCustomerService;
        $this->addressService = $addressService;
        $this->storeService = $storeService;
        $this->providerFactory = $providerFactory;
        $this->paymentDataService = $paymentDataService;
        $this->membershipService = $membershipService;
        $this->membershipTypeGroupRepository = $membershipTypeGroupRepository;
        $this->vatIdService = $vatIdService;
        $this->countryRepository = $countryRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getAction(string $publicId)
    {
        $possibleCustomer = $this->possibleCustomerRepository->findOneBy(['publicId' => $publicId]);

        if (null === $possibleCustomer) {
            throw new NotFoundHttpException('Invalid ID: ' . $publicId);
        }

        return $this->handleViewAndSetETag($this->view(['data' => $possibleCustomer]));
    }

    /**
     * @param string $publicId
     * @param string $phone
     *
     * @throws \App\Exception\InvalidPhoneNumberException
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postVoucherAction(string $publicId, string $phone)
    {
        $phone = PhoneNumberUtility::sanitize($phone);
        $possibleCustomer = $this->possibleCustomerRepository->findOneBy(['publicId' => $publicId]);
        $this->possibleCustomerMessageService->connectOrCreateUser($phone, $possibleCustomer);
//        $this->possibleCustomerMessageService->sendVoucher($phone, $possibleCustomer);

        return $this->createNoContentResponse();
    }

    /**
     * @Rest\Get("/possible-customer/by-public-id/{publicId}")
     *
     * @param string $publicId
     */
    public function getByPublicIdAction(string $publicId)
    {
        $possibleCustomer = $this->possibleCustomerRepository->findOneBy(['publicId' => $publicId]);

        if (!empty($possibleCustomer)) {
            return $this->handleView($this->view(['data' => $possibleCustomer]));
        }

        throw new NotFoundHttpException('No content');
    }

    /**
     * @Rest\Post("/possible-customer/{id}/convert-to-customer")
     *
     * @param PossibleCustomer $possibleCustomer
     * @param Request          $request
     *
     * @throws \App\Exception\InvalidPhoneNumberException
     * @throws NotFoundHttpException
     *
     * @return Response
     */
    public function postConvertToCustomerAction(PossibleCustomer $possibleCustomer, Request $request)
    {
        if ($possibleCustomer->getUser() !== null) {
            return $this->handleView($this->view(['data' => 'Possible customer already has an user account'], Response::HTTP_CONFLICT));
        }

        $requestData = $request->request->get('data');

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection([
            'phone' => new Assert\NotBlank(),
            'firstName' => new Assert\NotBlank(),
            'lastName' => new Assert\NotBlank(),
            'email' => new Assert\Email(),
            'company' => new Assert\NotBlank(),
            'vatId' => new Assert\Required(),
            'website' => new Assert\Url(),
            'description' => new Assert\Required(),
            'tos' => new Assert\Required(),
            'privacyPolicy' => new Assert\Required(),
            'companyPhone' => new Assert\NotBlank(),
            'advertiser' => new Assert\Required(),
            'companyEmail' => new Assert\Email(),
            'couponRedeemNotification' => new Assert\Required(),
            'dailyCouponRedeemOverview' => new Assert\Required(),
            'weeklyCouponRedeemOverview' => new Assert\Required(),
            'monthlyCouponRedeemOverview' => new Assert\Required(),
            'billingAddress' => new Assert\Required(),
            'isBonusCardEnabled' => new Assert\Required(),
            'membershipTypeGroup' => [new Assert\NotBlank(), new Assert\Type(['type' => 'integer'])],
            'country_id' => [new Assert\NotBlank(), new Assert\Type(['type' => 'integer'])],
            'company_street' => new Assert\NotBlank(),
            'company_zip' => new Assert\NotBlank(),
            'company_city' => new Assert\NotBlank(),
            'hasOpeningTimes' => new Assert\Required(),
            'openingTimes' => !empty($requestData['hasOpeningTimes']) ? new Assert\NotBlank() : new Assert\Required(),
            'openingTimesComment' => new Assert\Required(),
            'iban' => [new Assert\NotBlank(), new Assert\Iban()],
            'publicId' => new Assert\NotBlank(),
        ]);

        $violations = $validator->validate($requestData, $constraint);

        if (0 !== count($violations)) {
            return $this->handleView($this->view(['data' => $violations], Response::HTTP_BAD_REQUEST));
        }

        $requestData['phone'] = PhoneNumberUtility::sanitize($requestData['phone']);
        $requestData['companyPhone'] = PhoneNumberUtility::sanitize($requestData['companyPhone']);

        if (!empty($requestData['vatId']) && !$this->vatIdService->isValid($requestData['vatId'], true)) {
            throw new InvalidVatIdException('VAT ID is invalid.');
        }

        if (empty($requestData['billingAddress']) && ($requestData['billingAddress'] !== null)) {
            return $this->handleView($this->view(['data' => 'Billing address is invalid'], Response::HTTP_BAD_REQUEST));
        }

        $possibleCustomer = $this->possibleCustomerRepository->findOneBy(['id' => $possibleCustomer, 'publicId' => $requestData['publicId']]);

        if (empty($possibleCustomer)) {
            return $this->handleView($this->view(['data' => 'You can not create a customer'], Response::HTTP_UNAUTHORIZED));
        }

        $user = $this->userRepository->findOneBy(['phone' => $requestData['phone']]);

        if (!empty($user)) {
            return $this->handleView($this->view(['data' => 'The user already exists'], Response::HTTP_CONFLICT));
        }

        $requestDataBillingAddress = $requestData['billingAddress'];
        unset($requestData['billingAddress']);

        $this->em->getConnection()->beginTransaction();

        try {
            $membershipTypeGroup = $this->membershipTypeGroupRepository->find($requestData['membershipTypeGroup']);

            if (null === $membershipTypeGroup) {
                throw new NotFoundHttpException('No membership type with ID ' . $requestData['membershipTypeGroup'] . ' found');
            }

            $addressData = $this->addressService->prepareAddressDataAddCustomer($requestData);
            $address = $this->addressService->saveAddress($addressData);

            if ($requestDataBillingAddress === null) {
                $dataBillingAddress = [
                    'company' => $requestData['company'],
                    'name' => '',
                    'address' => [
                        'street' => $requestData['company_street'],
                        'zip' => $requestData['company_zip'],
                        'city' => $requestData['company_city'],
                        'country' => $requestData['country_id'],
                    ],
                ];
            } else {
                $dataBillingAddress = [
                    'company' => $requestDataBillingAddress['company'],
                    'name' => $requestDataBillingAddress['name'],
                    'address' => [
                        'street' => $requestDataBillingAddress['address']['street'],
                        'zip' => $requestDataBillingAddress['address']['zip'],
                        'city' => $requestDataBillingAddress['address']['city'],
                    ],
                ];

                $countryBillingAddress = $this->countryRepository->findOneBy(['isoAlpha2' => $requestDataBillingAddress['address']['country']]);

                if (empty($countryBillingAddress)) {
                    return $this->handleView($this->view(['data' => 'Billing address is invalid'], Response::HTTP_BAD_REQUEST));
                }

                $dataBillingAddress['address']['country'] = $countryBillingAddress->getId();
            }

            $billingAddress = new BillingAddress();
            $form = $this->createForm(BillingAddressType::class, $billingAddress);
            $form->submit($dataBillingAddress);

            if (false === $form->isValid()) {
                return $this->handleView($this->view(['data' => 'Billing address is invalid'], Response::HTTP_BAD_REQUEST));
            }

            $this->em->persist($form->getData());
            $this->em->flush();

            $userData = $this->possibleCustomerService->prepareCustomerDataAddCustomer($requestData, $billingAddress);
            $user = $this->possibleCustomerService->saveCustomer($userData, $possibleCustomer);

            $storeData = $this->storeService->prepareStoreDataAddCustomer($user, $address, $requestData);
            $this->storeService->saveStore($user, $address, $storeData);

            $this->storeService->updateCoordinates($address);

            $paymentData = $this->paymentDataService->preparePaymentDataAddCustomer($requestData);
            $provider = $this->providerFactory->create('iban');
            $provider->addOrUpdateCustomer(
                $user,
                $paymentData['type'],
                $paymentData['ownerName'],
                $paymentData['data']
            );

            $this->membershipService->updateMembershipByMembershipTypeGroup($user, $membershipTypeGroup);

            $this->em->getConnection()->commit();

            $this->eventDispatcher->dispatch(CreatingActiveCustomerEvent::NAME, new CreatingActiveCustomerEvent($user));
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();

            throw $e;
        }

        return $this->handleView($this->view(['data' => $user], Response::HTTP_CREATED));
    }
}
